// chandra keerthi vidyadharani
// c0741753
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BlazeDemoTest {
	
	WebDriver driver;
	final String driverLocation = "/Users/sm/Desktop/chromedriver";
	
	final String uRL= "http://blazedemo.com/";

	@Before
	public void setUp() throws Exception {
		// setup selenium and webdriver
		System.setProperty("webdriver.chrome.driver", 
				driverLocation);
	    driver = new ChromeDriver();
		//// go to website
		driver.get(uRL);

	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.close();
		
	}

	@Test
	public void tc1() {
		//
		WebElement departCities = driver.findElement(By.name("fromPort"));
		Select listBox = new Select(departCities);
		int count = listBox.getOptions().size();
		//checking if there are 7 departure cities
		assertEquals(count,7);
	}
	@Test
	public void tc2() {
		// press submit button
		WebElement findflights = driver.findElement(By.cssSelector("input[type='submit']"));
		findflights.click();
//		WebElement virginAmerica = driver.findElement(By.cssSelector("form[name='VA12']"));
		
	}
	

}
