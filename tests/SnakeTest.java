// chandra keerthi vidyadharani
// c0741753

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {
// declaring snakes	
	Snake snake1 ;
	Snake snake2 ;

	@Before
	public void setUp() throws Exception {	
// creating snakes		
		snake1 = new Snake("Peter",10,"coffee");
		snake2 = new Snake("Takis",80,"vegetables");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void snakeshealthytest() {
// testing if the snake 1 is healthy
		assertTrue(snake1.isHealthy());
		// testing if the snake 1 is not healthy		
		assertFalse(snake1.isHealthy());
// testing if the snake 2 is healthy	
		assertTrue(snake2.isHealthy());	
		// testing if the snake 1 is not healthy
		assertFalse(snake2.isHealthy());

		
	}

	@Test
	public void snake1lengthtest() {
// testing if snake 1 fits the cage
		//if the snake length is less than cage length
		assert(snake1.fitsInCage(20));
		//if the snake length is equal to cage length
		assert(snake1.fitsInCage(10));
		//if the snake length is greater than cage length
		assert(snake1.fitsInCage(5));

	}
	

}
